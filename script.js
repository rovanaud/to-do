
var visible = ["#detail0"];

var check = (event) => {
	event.target.classList.toggle('checked');
}

var refresh = (event, ...nodes) => {
	// don't do anything if it's the button which has been clicked
	if (event.target.tagName === 'BUTTON') {
		args = nodes;
		visible[0] = '#' + args.shift().id.replace('task', 'detail');
		args[0].classList.add('active');
		document.querySelector(visible[0]).style.display = '';
		return;
	}

	document.querySelector(visible[0]).style.display = 'none';
	document.querySelector(visible[0].replace('detail', 'task')).classList.remove('active');
	visible[0] = '#' + event.target.id.replace('task', 'detail');

	document.querySelector(visible[0]).style.display = '';
	event.target.classList.add('active');
}

function shift(elmt){
	var regex1 = /[a-z]+/;
	var regex2 = /[0-9]+/;
	var tmp = parseInt(elmt.id.replace(regex1, '')) - 1;
	elmt.setAttribute('id', elmt.id.replace(regex2, tmp.toString()));
}


// ajouter une tâche à une liste ok!
function newTask(event){
	input = document.querySelector('#' + event.target.parentNode.parentNode.id + ' input');
	text = input.value;
	if (text === '')
		alert('Write something');
	else {
		var li = document.createElement("li");
		li.appendChild(document.createTextNode(text));
		button = document.createElement('button');
		button.appendChild(document.createTextNode('x'));
		button.classList.add('close');
		button.setAttribute('type', 'button');
		button.setAttribute('onclick', 'del(event)');
		li.appendChild(button);
		li.addEventListener('click', check);
		document.querySelector('#' + event.target.parentNode.parentNode.id + ' ul').appendChild(li);
		input.value = '';
	}
}

// ajouter une liste de tâches
function newList(){
	var input = document.getElementById("title");
	
	// ajouter le nouvel élément.
	var li = document.createElement("li");
	number = document.querySelectorAll("#todoList ul li").length.toString();
	li.id = "task" + number;
	text = input.value;
	li.appendChild(document.createTextNode(text));
	button = document.createElement('button');
	button.appendChild(document.createTextNode('x'));
	button.classList.add('close');
	button.setAttribute('type', 'button');
	button.setAttribute('onclick', 'del(event)');
	li.appendChild(button);
	//select the new group and deselect the older
	document.querySelector(visible[0].replace('detail', 'task')).classList.remove('active');
	li.classList.add('active');
	document.querySelector("#todoList ul").appendChild(li);
	
	input.value = '';

	// create associated tasks list
	div = document.createElement('div');
	div.id = "detail" + number;
	div.classList.add('group');

	divHeader = document.createElement('div');
	divHeader.classList.add('header');
	header = document.createElement('h3');
	header.appendChild(document.createTextNode(text));
	divHeader.appendChild(header);
	div.appendChild(divHeader);

	ul = document.createElement('ul');
	div.appendChild(ul);

	input = document.createElement('input');
	input.classList.add('input');
	input.setAttribute('placeholder', 'New Task ...');
	divHeader.appendChild(input);

	span = document.createElement('span');
	span.appendChild(document.createTextNode('+'));
	span.classList.add('validate');
	span.setAttribute('onclick', "newTask(event)");
	divHeader.appendChild(span);

	document.querySelector("#taskList").appendChild(div);

	// add click listener add hide current group
	document.querySelector(visible[0]).style.display = 'none';
	visible[0] = '#' + div.id.replace('task', 'detail');
	li.addEventListener('click', refresh);
}


window.onload = (event) => {
	document.querySelectorAll(".group").forEach((elmt) => {
		elmt.style.display = 'none';
	});
	document.querySelectorAll('#todoList ul li').forEach((elmt) => {
		elmt.addEventListener('click', refresh);
	});
	document.querySelectorAll('.group ul li').forEach((element) => {
		element.addEventListener('click', check);
	});
	document.querySelector(visible[0]).style.display = '';
	document.querySelector(visible[0].replace('detail', 'task')).classList.add('active');
}


function del(event){
	var li = event.target.parentNode;
	var id = li.id;
	li.parentNode.removeChild(li);
	if(id.indexOf("task") !== -1){
		//delete the corresponding tasks list
		detail = document.querySelector("#" + id.replace('task', 'detail'));
		detail.parentNode.removeChild(detail);

		next = parseInt(id.replace('task', ''));
		//shift other groups
		next++;
		temp = document.querySelector('#task' + next.toString());
		while(temp){
			shift(temp);
			shift(document.querySelector('#detail' + next.toString()));
			next++;
			temp = document.querySelector('#task' + next.toString());
		}
		refresh(event, document.querySelector('#task0'));
	}
}